import React from 'react';
import { StyleSheet, Text, SafeAreaView, ActivityIndicator, FlatList, TouchableOpacity, TextInput, Pressable, Modal, View, Alert } from 'react-native';

const Item = ({ item, onPress, backgroundColor, textColor }) => (
  <TouchableOpacity onPress={onPress} style={[styles.item, backgroundColor]}>
    <Text style={[styles.texte, textColor]}>{item.nom}, {item.email}</Text>
  </TouchableOpacity>
);

export default class FlatListSQL extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true,
      selectedId: '',
      selectedLogin: '',
      selectedEmail: '',
      selectedNom: '',
      selectedPrenom: '',
      selectedAdresse: '',
      selectedCodePostal: '',
      selectedVille: '',
      modalVisible: false,
      login: '',
      email: '',
      password: '',
      nom: '',
      prenom: '',
      adresse: '',
      codePostal: '',
      ville: '',
      PREFIX_salt: 'azerty',
      SUFFIX_salt: 'poiuyt',
    };
  }

  async componentDidMount() {
    const response = await fetch('http://192.168.1.49:3000/all');
    const client = await response.json();
    this.setState({ data: client, isLoading: false });

  }

  renderItem({ item }) {
    const backgroundColor = item.id === this.state.selectedId ? "#6e3b6e" : "#ff9c2f";
    const color = item.id === this.state.selectedId ? 'white' : 'black';

    return (
      <Item
        item={item}
        onPress={() => this.setState({ selectedId: item.id, selectedLogin: item.login, selectedEmail: item.email, selectedNom: item.nom, selectedPrenom: item.prenom, selectedAdresse: item.adresse, selectedCodePostal: item.cp, selectedVille: item.ville })}
        backgroundColor={{ backgroundColor }}
        textColor={{ color }}
      />
    );
  };

  UserUpdateFunction(id) {

    const {modalVisible} = this.state;

    fetch('http://192.168.1.49:3000/update', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'nom': this.state.nom == '' ? this.state.selectedNom : this.state.nom,
        'email': this.state.email == '' ? this.state.selectedEmail : this.state.email,
        'prenom': this.state.prenom == '' ? this.state.selectedPrenom : this.state.prenom,
        'adresse': this.state.adresse == '' ? this.state.selectedAdresse : this.state.adresse,
        'codePostal': this.state.codePostal == '' ? this.state.selectedCodePostal : this.state.codePostal,
        'ville': this.state.ville == '' ? this.state.selectedVille : this.state.ville,
        'id': id,
      })

    }).then((response) => response.text())
      .then((responseText) => {
        // Showing response message coming from server after inserting records.
        Alert.alert(responseText);
      })
      .then(this.setState({ modalVisible: !modalVisible }))
      .catch((error) => {
        console.error(error);
      });

  }

  render() {
    const { data, isLoading, selectedId, selectedLogin, selectedEmail, selectedNom, selectedPrenom, selectedAdresse, selectedCodePostal, selectedVille, modalVisible } = this.state;

    return (
             
        <SafeAreaView style={styles.view}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
        >
          <View style={styles.modalView}>
          <Text style={styles.title}>Utilisateur</Text>
          <View>
            <TextInput
              style={styles.input}
              editable= {false}
              defaultValue= {selectedLogin}
            />
            <TextInput
              style={styles.input}
              placeholder='Email'
              textContentType='emailAddress'
              defaultValue= {selectedEmail}
              onChangeText={email => this.setState({ email })}
            />
            <TextInput
              style={styles.input}
              placeholder='Nom'
              textContentType='name'
              defaultValue= {selectedNom}
              onChangeText={nom => this.setState({ nom })}
            />
            <TextInput
              style={styles.input}
              placeholder='Prenom'
              textContentType='name'
              defaultValue= {selectedPrenom}
              onChangeText={prenom => this.setState({ prenom })}
            />
            <TextInput
              style={styles.input}
              placeholder='Adresse'
              textContentType='fullStreetAddress'
              defaultValue= {selectedAdresse}
              onChangeText={adresse => this.setState({ adresse })}
            />
            <TextInput
              style={styles.input}
              placeholder='Code postal'
              textContentType='postalCode'
              defaultValue= {selectedCodePostal}
              onChangeText={codePostal => this.setState({ codePostal })}
            />
            <TextInput
              style={styles.input}
              placeholder='Ville'
              textContentType='addressCity'
              defaultValue= {selectedVille}
              onChangeText={ville => this.setState({ ville })}
            />
          </View>
          <Pressable
            style={[styles.button, styles.buttonOpen]}
            onPress={this.UserUpdateFunction.bind(this, selectedId)}
          >
            <Text style={styles.textStyle}>Modifier</Text>
          </Pressable>
          <Pressable
            style={[styles.button, styles.buttonClose]}
            onPress={() => this.setState({ modalVisible: !modalVisible })}
          >
            <Text style={styles.textStyle}>Annuler</Text>
          </Pressable>
          </View>
        </Modal>
          {isLoading ? <ActivityIndicator /> : (
            <FlatList
              data={data}
              keyExtractor={(item) => item.id.toString()}
              renderItem={this.renderItem.bind(this)}
              extraData={selectedId}
            />
          )}
          <TouchableOpacity style={styles.buttonM} onPress={() => this.setState({ modalVisible: !modalVisible })}>
            <Text style={styles.texte}>Modifier</Text>
          </TouchableOpacity>
        </SafeAreaView>
      
    );
  }
}

const styles = StyleSheet.create({
  view: {
    margin: 20,
    flex: 1,
    alignItems: 'center',
  },
  title: {
    fontSize: 22,
    marginBottom: 20
  },
  texte: {
    fontSize: 14,
    margin: 5,
    alignItems: 'center',

  },
  item: {
    backgroundColor: '#ff9c2f',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 13,
  },
  buttonM: {
    marginBottom: 5,
    width: 260,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2196F3'
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    margin: 10
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  input: {
    height: 30,
    width: 280,
    margin: 5,
    borderWidth: 1,
  }
});