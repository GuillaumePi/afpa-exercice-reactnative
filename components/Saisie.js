import React, {Component} from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';

export default class Saisie extends Component {

state = {Email: ''}

handleEmail = (text) => {
    this.setState({Email: text})
}

Pop = (Email) => {
    alert('Email : ' + Email)
}

render(){
  return (
    <View style={styles.view}>
        <Text style={styles.title}>Email :</Text>
        <TextInput
        style={styles.input}
        onChangeText = {this.handleEmail}
        placeholder="Saisir votre Email..."
        />
        <TouchableOpacity
            style = {styles.button}
            onPress = {() => this.Pop(this.state.Email)}>
            <Text style = {styles.buttonText}> Sousmettre </Text>
        </TouchableOpacity>
        <Text style={styles.texte}>Adresse Email : {this.state.Email}</Text>
    </View>
  );
  }
}

const styles = StyleSheet.create({
  view: {
    margin: 20,
    flex: 1,
    alignItems: 'center',

  },
  title: {
    fontSize: 22,
    marginBottom: 20
  },
  input: {
      height: 40,
      width: 290,
      margin: 12,
      borderWidth: 1,
    },
    button: {
      marginBottom: 30,
      width: 180,
      height: 40,
      alignItems: 'center',
      backgroundColor: '#2196F3'
     },
    buttonText: {
          textAlign: 'center',
          padding: 10,
          color: 'white'
     },
     texte: {
          fontSize: 14,
          margin: 5
     },
});