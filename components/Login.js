import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  Alert,
  ToastAndroid,
} from 'react-native';
//import App from '../App';

// expo install expo-linear-gradient (For Expo Users)
// Alternate: npm i react-native-linear-gradient (For non-expo users)
import { LinearGradient } from 'expo-linear-gradient';

// npm install react-native-elements
import { Icon } from 'react-native-elements';

// https://fonts.google.com/specimen/Nunito+Sans
import * as Font from 'expo-font';
import md5 from 'md5';

let customFonts = {
  'NSLight': require('../assets/fonts/NunitoSans/NunitoSans-Light.ttf'),
  'NSRegular': require('../assets/fonts/NunitoSans/NunitoSans-Regular.ttf'),
  'NSBold': require('../assets/fonts/NunitoSans/NunitoSans-Bold.ttf'),
  'NSExtraBold': require('../assets/fonts/NunitoSans/NunitoSans-ExtraBold.ttf'),
}

export default class Login extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      activeTab: 'Login',
      fontsLoaded: false,
      showRegisterPassword: false,
      showLoginPassword: false,
      login: '',
      email: '',
      password: '',
      nom: '',
      prenom: '',
      adresse: '',
      codePostal: '',
      ville: '',
      PREFIX_salt: 'azerty',
      SUFFIX_salt: 'poiuyt',
      token: ''
    };


  }

  async _loadFontsAsync() {
    await Font.loadAsync(customFonts);
    this.setState({ fontsLoaded: true });
  }

  switchTab() {
    if (this.state.activeTab === 'Login') {
      this.setState({ activeTab: 'Register' });
    } else {
      this.setState({ activeTab: 'Login' });
    }
  }

  componentDidMount() {
    StatusBar.setBarStyle('light-content', true);
    this._loadFontsAsync();
  }

  UserLoginFunction(props) {
    let mdpHash = md5(this.state.PREFIX_salt + this.state.password + this.state.SUFFIX_salt);

    fetch('http://192.168.1.49:3000/login', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({

        'login': this.state.login,
        'password': mdpHash,
      })

    }).then((response) => response.text())
      .then((responseText) => {
        this.setState({ token: responseText });
        console.log(this.state.token);
      })
      .catch((error) => {
        console.error(error);
      });

    if (this.state.token !== '') {
      ToastAndroid.show("Vous êtes loggé !", ToastAndroid.SHORT);
    } else {
      ToastAndroid.show("Mauvais login ou mot de passe...", ToastAndroid.SHORT);
    }
  }

  UserRegistrationFunction() {

    let mdpHash = md5(this.state.PREFIX_salt + this.state.password + this.state.SUFFIX_salt);

    fetch('http://192.168.1.49:3000/register', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'nom': this.state.nom,
        'login': this.state.login,
        'email': this.state.email,
        'password': mdpHash,
        'prenom': this.state.prenom,
        'adresse': this.state.adresse,
        'codePostal': this.state.codePostal,
        'ville': this.state.ville,
      })

    }).then((response) => response.text())
      .then((responseText) => {
        // Showing response message coming from server after inserting records.
        Alert.alert(responseText);
      })
      .catch((error) => {
        console.error(error);
      });

  }

  Login() {
    const { showLoginPassword } = this.state;

    return (
      <View style={{ marginTop: 10 }}>
        <View style={styles.inputView}>
          <Icon
            style={{ paddingHorizontal: 4 }}
            name='user'
            type='font-awesome'
            color='#fff'
            size={22}
          />
          <TextInput
            style={styles.input}
            placeholder='Login'
            placeholderTextColor='#f1f2f6'
            keyboardType='default'
            textContentType='username'
            autoCapitalize={'none'}
            autoCompleteType='username'
            returnKeyType='next'
            onChangeText={login => this.setState({ login })}
          />
        </View>
        <View style={styles.inputView}>
          <Icon
            style={{ paddingHorizontal: 4 }}
            name='key'
            type='font-awesome-5'
            color='#fff'
            size={22}
          />
          <TextInput
            style={styles.input}
            placeholder='Password'
            placeholderTextColor='#f1f2f6'
            secureTextEntry={!showLoginPassword}
            textContentType='password'
            returnKeyType='done'
            onChangeText={password => this.setState({ password })}
          />
          <TouchableOpacity
            style={{ paddingVertical: 4 }}
            onPress={() => {
              this.setState({ showLoginPassword: !showLoginPassword });
            }}
          >
            <Icon
              style={{ paddingHorizontal: 4 }}
              name='eye'
              type='font-awesome'
              color='#fff'
              size={22}
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={styles.button} onPress={this.UserLoginFunction.bind(this)}>
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={styles.forgotPasswordText}>Mot de passe oublié?</Text>
        </TouchableOpacity>
      </View>
    );
  }

  Register() {
    const { showRegisterPassword } = this.state;

    return (
      <ScrollView style={{ marginTop: 10 }}>
        <View style={styles.inputView}>
          <Icon
            style={{ paddingHorizontal: 4, width: 30 }}
            name='user'
            type='font-awesome'
            color='#fff'
            size={22}
          />
          <TextInput
            style={styles.input}
            placeholder='Login'
            placeholderTextColor='#f1f2f6'
            textContentType='username'
            autoCompleteType='username'
            //returnKeyType='next'
            onChangeText={login => this.setState({ login })}

          />
        </View>
        <View style={styles.inputView}>
          <Icon
            style={{ paddingHorizontal: 4, width: 30 }}
            name='envelope'
            type='font-awesome'
            color='#fff'
            size={22}
          />
          <TextInput
            style={styles.input}
            placeholder='Email'
            placeholderTextColor='#f1f2f6'
            keyboardType='email-address'
            textContentType='emailAddress'
            autoCapitalize={'none'}
            autoCompleteType='email'
            //returnKeyType='next'
            onChangeText={email => this.setState({ email })}

          />
        </View>
        <View style={styles.inputView}>
          <Icon
            style={{ paddingHorizontal: 4, width: 30 }}
            name='key'
            type='font-awesome-5'
            color='#fff'
            size={22}
          />
          <TextInput
            style={styles.input}
            placeholder='Password'
            placeholderTextColor='#f1f2f6'
            secureTextEntry={!showRegisterPassword}
            textContentType='password'
            //returnKeyType='done'
            onChangeText={password => this.setState({ password })}

          />
          <TouchableOpacity
            style={{ paddingVertical: 4 }}
            onPress={() => {
              this.setState({ showRegisterPassword: !showRegisterPassword });
            }}
          >
            <Icon
              style={{ paddingHorizontal: 4 }}
              name='eye'
              type='font-awesome'
              color='#fff'
              size={22}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.inputView}>
          <Icon
            style={{ paddingHorizontal: 4, width: 30 }}
            name='user'
            type='font-awesome'
            color='#fff'
            size={22}
          />
          <TextInput
            style={styles.input}
            placeholder='Nom'
            placeholderTextColor='#f1f2f6'
            //returnKeyType='next'
            onChangeText={nom => this.setState({ nom })}
          />
        </View>
        <View style={styles.inputView}>
          <Icon
            style={{ paddingHorizontal: 4, width: 30 }}
            name='user'
            type='font-awesome'
            color='#fff'
            size={22}
          />
          <TextInput
            style={styles.input}
            placeholder='Prenom'
            placeholderTextColor='#f1f2f6'
            //returnKeyType='next'
            onChangeText={prenom => this.setState({ prenom })}

          />
        </View>
        <View style={styles.inputView}>
          <Icon
            style={{ paddingHorizontal: 4, width: 30 }}
            name='road'
            type='font-awesome'
            color='#fff'
            size={22}
          />
          <TextInput
            style={styles.input}
            placeholder='Adresse'
            placeholderTextColor='#f1f2f6'
            textContentType='fullStreetAddress'
            // returnKeyType='next'
            onChangeText={adresse => this.setState({ adresse })}

          />
        </View>
        <View style={styles.inputView}>
          <Icon
            style={{ paddingHorizontal: 4, width: 30 }}
            name='road'
            type='font-awesome'
            color='#fff'
            size={22}
          />
          <TextInput
            style={styles.input}
            placeholder='Code postal'
            placeholderTextColor='#f1f2f6'
            textContentType='postalCode'
            //returnKeyType='next'
            onChangeText={codePostal => this.setState({ codePostal })}

          />
        </View>
        <View style={styles.inputView}>
          <Icon
            style={{ paddingHorizontal: 4, width: 30 }}
            name='road'
            type='font-awesome'
            color='#fff'
            size={22}
          />
          <TextInput
            style={styles.input}
            placeholder='Ville'
            placeholderTextColor='#f1f2f6'
            textContentType='addressCity'
            //returnKeyType='next'
            onChangeText={ville => this.setState({ ville })}

          />
        </View>
        <TouchableOpacity style={styles.button} onPress={this.UserRegistrationFunction.bind(this)}>
          <Text style={styles.buttonText}>Register</Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }

  render() {
    const { userToken } = this.props;

    if (!this.state.fontsLoaded) {
      return (
        <View>
          <Text>Loading...</Text>
        </View>
      );
    }

    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <LinearGradient colors={['#E44D26', '#F16529']} style={styles.container}>
          <Text style={styles.welcomeText}>
            {this.state.activeTab === 'Login' ? 'Bienvenue !' : 'Inscrivez-vous'}
          </Text>
          <View style={styles.switchTabsView}>
            <TouchableOpacity
              style={{
                borderBottomWidth: this.state.activeTab === 'Login' ? 4 : 0,
                borderBottomColor: '#fff',
                paddingHorizontal: 4,
                marginRight: 14,
              }}
              onPress={() => this.switchTab()}
            >
              <Text style={styles.switchText}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                borderBottomWidth: this.state.activeTab === 'Register' ? 4 : 0,
                borderBottomColor: '#fff',
                paddingHorizontal: 4,
                marginRight: 14,
              }}
              onPress={() => this.switchTab()}
            >
              <Text style={styles.switchText}>Register</Text>
            </TouchableOpacity>
          </View>
          {this.state.activeTab === 'Login' ? this.Login() : this.Register()}
        </LinearGradient>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 40,
  },
  welcomeText: {
    alignSelf: 'center',
    fontSize: 40,
    fontFamily: 'NSLight',
    marginTop: 10,
    color: '#fff',
  },
  switchTabsView: {
    display: 'flex',
    flexDirection: 'row',
    paddingHorizontal: 20,
    marginTop: 20,
  },
  switchText: {
    padding: 2,
    fontSize: 20,
    color: '#fff',
    fontFamily: 'NSExtraBold',
  },
  inputView: {
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: '#fff',
    marginTop: 10,
    marginHorizontal: 20,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    flex: 1,
    height: 40,
    fontSize: 16,
    fontFamily: 'NSLight',
    paddingHorizontal: 4,
    color: '#fff',
  },
  button: {
    marginHorizontal: 20,
    backgroundColor: '#fafafa',
    marginTop: 12,
    paddingVertical: 10,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  buttonText: { fontFamily: 'NSRegular', fontSize: 16, color: '#E44D26' },
  forgotPasswordText: {
    marginHorizontal: 20,
    marginTop: 20,
    alignSelf: 'flex-end',
    color: '#fff',
    fontSize: 18,
    fontFamily: 'NSBold',
  },
  socialLoginView: {
    marginTop: 40,
    marginHorizontal: 20,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  socialLoginTouchable: {
    backgroundColor: '#fff',
    width: 40,
    height: 40,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 8,
  },
});