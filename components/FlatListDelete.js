import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, FlatList, Alert, Modal, Pressable } from 'react-native';



export default class FlatListDelete extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            isLoading: true,
            modalVisible: false,
            id: ''
        };


    }

    async componentDidMount() {
        const response = await fetch('http://192.168.1.49:3000/all');
        const client = await response.json();
        this.setState({ data: client, isLoading: false });

    }

    async refresh() {

        const {modalVisible} = this.state;
        const response = await fetch('http://192.168.1.49:3000/all');
        const client = await response.json();
        this.setState({ data: client, modalVisible: !modalVisible });
    }

    UserDeleteFunction(id) {
        fetch('http://192.168.1.49:3000/delete', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'id': id,
            })

        }).then((response) => response.text())
            .then((responseText) => {
                // Showing response message coming from server after inserting records.
                Alert.alert(responseText);
            })
            .then(this.refresh.bind(this))
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        const { data, isLoading, modalVisible, id } = this.state;

        return (

            <View style={styles.centeredView}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                >
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>Etes vous sûr de vouloir supprimmer cet utilisateur?</Text>
                        <Pressable
                            style={[styles.button, styles.buttonOpen]}
                            onPress={this.UserDeleteFunction.bind(this, id)}
                        >
                            <Text style={styles.textStyle}>Supprimer</Text>
                        </Pressable>
                        <Pressable
                            style={[styles.button, styles.buttonClose]}
                            onPress={() => this.setState({modalVisible: !modalVisible})}
                        >
                            <Text style={styles.textStyle}>Annuler</Text>
                        </Pressable>                       
                    </View>
                </Modal>

                <View style={styles.view}>
                    {isLoading ? <ActivityIndicator /> : (

                        <FlatList
                            data={data}
                            //refreshing= {true}
                            //onRefresh= {this.refresh.bind(this)}
                            keyExtractor={(item) => item.id.toString()}
                            renderItem={({ item }) => (
                                <TouchableOpacity onPress={() => this.setState({modalVisible: !modalVisible, id: item.id})} style={styles.item}>
                                    <Text>{item.prenom} {item.nom}</Text>
                                </TouchableOpacity>
                            )}
                        />
                    )}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        padding: 24,
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    title: {
        fontSize: 22,
        marginBottom: 20
    },
    texte: {
        fontSize: 14,
        margin: 5
    },
    item: {
        backgroundColor: '#ff9c2f',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 13,
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
      },
      button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        margin: 10
      },
      buttonOpen: {
        backgroundColor: "#F194FF",
      },
      buttonClose: {
        backgroundColor: "#2196F3",
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center"
      }
});