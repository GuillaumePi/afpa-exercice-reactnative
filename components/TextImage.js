import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';

export default class TextImage extends React.Component {
render(){
  return (
    <View style={styles.view}>
      <Text style={styles.title}>Du texte...</Text>
      <Text style={{fontStyle: 'italic'}}>lorem ipsum dolor sit amet, consectetur</Text>
      <Text style={{fontWeight: 'bold'}}>lorem ipsum dolor sit amet, consectetur</Text>
      <Text style={{color: 'red'}}>lorem ipsum dolor sit amet, consectetur</Text>
      <Text style={{textDecorationLine: 'underline'}}>lorem ipsum dolor sit amet, consectetur</Text>
      <Text style={styles.title}>... et des images !</Text>
      <ScrollView horizontal>
        <Image style={styles.image} source={require('../assets/Emoji.png')} />
        <Image style={styles.image} source={require('../assets/EmojiN.png')} />
        <Image style={styles.image} source={require('../assets/Emoji.png')} />
        <Image style={styles.image} source={require('../assets/EmojiN.png')} />
        <Image style={styles.image} source={require('../assets/Emoji.png')} />
        <Image style={styles.image} source={require('../assets/EmojiN.png')} />
      </ScrollView>
    </View>
  );
  }
}

const styles = StyleSheet.create({
  view: {
    margin: 20,
    flex: 1,
    alignItems: 'center',

  },
  title: {
    fontSize: 22,
    marginBottom: 20,
    marginTop: 20
  },
  image: {
    margin: 15,
    width: 100,
    height: 100,

  }
});