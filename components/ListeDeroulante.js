import React from 'react';
import { StyleSheet, Text, View, Picker } from 'react-native';

export default class ListeDeroulante extends React.Component {

  state = { users: '' };

  updateUser = user => {
    this.setState({ users: user });
  };

  render() {
    return (
      <View style={styles.view}>
        <Text style={styles.title}>Une belle liste déroulante :</Text>
        <View style={styles.picker}>
          <Picker
            selectedValue={this.state.users}
            style={{ height: 50, width: 175 }}
            onValueChange={this.updateUser}
            mode="dialog">
            <Picker.Item label="React Native" value="React Native" />
            <Picker.Item label="Flutter" value="Flutter" />
            <Picker.Item label="Kotlin" value="Kotlin" />
            <Picker.Item label="Swift" value="Swift" />
          </Picker>
        </View>
        <Text style={styles.texte}>Vous avez choisi {this.state.users}.</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  view: {
    margin: 20,
    flex: 1,
    alignItems: 'center',
  },
  title: {
    fontSize: 22,
    marginBottom: 20
  },
  picker: {
    margin: 10,
    elevation: 1,
    borderWidth: 1
  },
  texte: {
    fontSize: 14,
    margin: 5
  },
});