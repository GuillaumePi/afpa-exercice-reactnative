import React, { useState } from 'react';
import { StyleSheet, Text, View, Picker, ActivityIndicator, FlatList } from 'react-native';

export default class ListeDeroulanteSQL extends React.Component {

constructor(props) {
    super(props);

    this.state = {
      emails: [],
      isLoading: true
    };
  }

async componentDidMount() {
/*
    await fetch('http://192.168.1.49:3000/emails')
      .then(response => response.json())
      .then(json => {
        this.setState({ emails: json.email });
      })
      .catch((error) => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
      */
      const response = await fetch('http://192.168.1.49:3000/emails');
      const client = await response.json();
      this.setState({emails: client, isLoading: false});
      console.log(client);
}

updateEmail = email => {
    this.setState({emails: email});
};

render(){
const { emails, isLoading } = this.state;
//const emailsArray = JSON.parse(JSON.stringify(this.state.emails));
  return (

    <View style={styles.view}>
        <Text style={styles.title}>Une belle liste déroulante :</Text>
         {isLoading ? <ActivityIndicator/> : (
            <View style={styles.picker}>
                <Picker
                    selectedValue={this.state.emails}
                    style={{ height: 50, width: 175 }}
                    onValueChange={this.updateEmail}
                    mode="dialog">
                    {
                    this.state.emails.map(email =>(<Picker.Item label={email} value={email} />))

                    }


                </Picker>
            </View>
            )}
        <Text style={styles.texte}>Vous avez choisi {this.state.emails}.</Text>
    </View>

  );
  }
}

const styles = StyleSheet.create({
  view: {
    margin: 20,
    flex: 1,
    alignItems: 'center',
  },
  title: {
    fontSize: 22,
    marginBottom: 20
  },
  picker: {
    margin: 10,
    elevation: 1,
    borderWidth: 1
  },
  texte: {
      fontSize: 14,
      margin: 5
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 13,
  }
});