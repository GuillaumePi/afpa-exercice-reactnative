const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const cors = require('cors')

const connection = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'php'
});

const app = express();
app.use(cors());
//app.use(bodyParser.urlencoded({ extended: false }));
//app.use(bodyParser.json());
app.use(express.json());

// Creating a GET route that returns data from the 'users' table.
app.get('/user/:id', function (req, res) {
  let idUser = req.params.id;
  // Connecting to the database.
  connection.getConnection(function (err, connection) {

    // Executing the MySQL query (select all data from the 'users' table).
    connection.query("SELECT * FROM `client` WHERE `id` ='" + idUser + "';", function (error, results, fields) {
      // If some error occurs, we throw an error.
      if (error) throw error;

      // Getting the 'response' from the database and sending it to our route. This is were the data is.
      res.send(results)
    });
  });
});

app.get('/all', function (req, res) {
  // Connecting to the database.
  connection.getConnection(function (err, connection) {

    // Executing the MySQL query (select all data from the 'users' table).
    connection.query('SELECT * FROM client', function (error, results, fields) {
      // If some error occurs, we throw an error.
      if (error) throw error;

      // Getting the 'response' from the database and sending it to our route. This is were the data is.
      res.send(results)
    });
  });
});

app.post('/login', function (req, res) {
  console.log(req.body);
  let login = req.body.login;
  let password = req.body.password;
  // Connecting to the database.
  connection.getConnection(function (err, connection) {

    // Executing the MySQL query (select all data from the 'users' table).
    connection.query("SELECT * FROM `client` WHERE `login` ='" + login + "' AND `mdp` = '" + password + "';", function (error, results, fields) {
      // If some error occurs, we throw an error.
      if (error) throw error;

      if (results !== '')
      res.send('Token')
    });
  });
});

app.post('/register', function (req, res) {
  console.log(req.body);

  let newUser = {
    Login: req.body.login,
    Email: req.body.email,
    Password: req.body.password,
    Nom: req.body.nom,
    Prenom: req.body.prenom,
    Adresse: req.body.adresse,
    CodePostal: req.body.codePostal,
    Ville: req.body.ville,
  };

  connection.getConnection(function (err, connection) {

    // Executing the MySQL query (select all data from the 'users' table).
    connection.query("INSERT INTO `client` (`id`, `nom`, `prenom`, `email`, `adresse`, `cp`, `ville`, `login`, `mdp`, `role`) VALUES (NULL,'" + newUser.Nom + "', '" + newUser.Prenom + "','" + newUser.Email + "', '" + newUser.Adresse + "', '" + newUser.CodePostal + "', '" + newUser.Ville + "', '" + newUser.Login + "', '" + newUser.Password + "', default);", function (error, results, fields) {
      // If some error occurs, we throw an error.
      if (error) throw error;

      // Getting the 'response' from the database and sending it to our route. This is were the data is.
      res.send('Nouvel utilisateur créé !')
    });
  });
});

app.post('/delete', function (req, res) {
  //console.log(req.body);

  let idUser = req.body.id;

  connection.getConnection(function (err, connection) {

    connection.query("DELETE FROM `client` WHERE `id` ='" + idUser + "';", function (error, results, fields) {
      // If some error occurs, we throw an error.
      if (error) throw error;

      // Getting the 'response' from the database and sending it to our route. This is were the data is.
      res.send('Utilisateur supprimé !')
    });
  });
});

app.post('/update', function (req, res) {
  console.log(req.body);

  let user = {
    Id: req.body.id,
    Email: req.body.email,
    Nom: req.body.nom,
    Prenom: req.body.prenom,
    Adresse: req.body.adresse,
    CodePostal: req.body.codePostal,
    Ville: req.body.ville,
  };

  connection.getConnection(function (err, connection) {

    // Executing the MySQL query (select all data from the 'users' table).
    connection.query("UPDATE `client` SET `nom` = '" + user.Nom + "', `prenom` = '" + user.Prenom + "', `email` = '" + user.Email + "', `adresse` = '" + user.Adresse + "', `cp` = '" + user.CodePostal + "', `ville` = '" + user.Ville + "' WHERE `id` = '" + user.Id + "';", function (error, results, fields) {
      // If some error occurs, we throw an error.
      if (error) throw error;

      // Getting the 'response' from the database and sending it to our route. This is were the data is.
      res.send('Utilisateur modifié !')
    });
  });
});

// Starting our server.
app.listen(3000, () => {
  console.log('Allez sur http://localhost:3000/all pour voir les données.');
});