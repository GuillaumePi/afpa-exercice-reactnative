import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, TouchableHighlight } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import 'react-native-gesture-handler';
import TextImage from './components/TextImage';
import Saisie from './components/Saisie';
import ListeDeroulante from './components/ListeDeroulante';
import FlatListSQL from './components/FlatListSQL';
import Login from './components/Login';
import Delete from './components/FlatListDelete';

function HomeScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Bienvenue !</Text>
      <TouchableHighlight onPress={() => navigation.navigate("Texte et Image")} underlayColor="white">
        <View style={styles.button}>
          <Text style={styles.buttonText}>Texte et image</Text>
        </View>
      </TouchableHighlight>
      <TouchableHighlight onPress={() => navigation.navigate("Saisie de texte")} underlayColor="white">
        <View style={styles.button}>
          <Text style={styles.buttonText}>Saisie de texte</Text>
        </View>
      </TouchableHighlight>
      <TouchableHighlight onPress={() => navigation.navigate("Liste déroulante")} underlayColor="white">
        <View style={styles.button}>
          <Text style={styles.buttonText}>Liste déroulante</Text>
        </View>
      </TouchableHighlight>
      <TouchableHighlight onPress={() => navigation.navigate("FlatList SQL")} underlayColor="white">
        <View style={styles.button}>
          <Text style={styles.buttonText}>FlatList SQL</Text>
        </View>
      </TouchableHighlight>
      <TouchableHighlight onPress={() => navigation.navigate("Login")} underlayColor="white">
        <View style={styles.button}>
          <Text style={styles.buttonText}>Login</Text>
        </View>
      </TouchableHighlight>
      <TouchableHighlight onPress={() => navigation.navigate("Delete")} underlayColor="white">
        <View style={styles.button}>
          <Text style={styles.buttonText}>Delete</Text>
        </View>
      </TouchableHighlight>
      <StatusBar style="auto" />
    </View>
  );
}

const Stack = createStackNavigator();

export default class App extends React.Component {
  constructor(props) {
    super(props);
    console.log(props);
  }
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Ma première application">
          <Stack.Screen name="Ma première application" component={HomeScreen} />
          {this.props.token == '' ? (
            <>
              <Stack.Screen name="Login" component={Login} />
            </>
          ) : (
            <>
              <Stack.Screen name="Texte et Image" component={TextImage} />
              <Stack.Screen name="Saisie de texte" component={Saisie} />
              <Stack.Screen name="Liste déroulante" component={ListeDeroulante} />
              <Stack.Screen name="FlatList SQL" component={FlatListSQL} />
              <Stack.Screen name="Delete" component={Delete} />
            </>
          )}

        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    marginBottom: 30,
    width: 260,
    alignItems: 'center',
    backgroundColor: '#2196F3'
  },
  buttonText: {
    textAlign: 'center',
    padding: 20,
    color: 'white'
  },
  title: {
    margin: 15,
    fontSize: 14
  }
});
